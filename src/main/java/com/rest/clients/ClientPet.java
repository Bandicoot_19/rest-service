package com.rest.clients;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Tag;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class ClientPet {

    private static ObjectMapper objectMapper;
    private static HttpResponse response;

    public ClientPet() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
    }

    public void Update(ClientHttp clientHttp, Pet pet) throws IOException, InterruptedException {
        String serializedPet = objectMapper.writeValueAsString(pet);
        response = clientHttp.makePutRequest("", serializedPet);
    }

    public void addNew(ClientHttp clientHttp, Pet pet) throws IOException, InterruptedException {
        String result = objectMapper.writeValueAsString(pet);
        response = clientHttp.makePostRequest("", result);
    }

    public List<Pet> findByStatus(ClientHttp clientHttp, String status) throws IOException, InterruptedException {
        try {
            List<Pet> pets = new ArrayList<>();
            response = clientHttp.makeGetRequest("/findByStatus?status=" + status);
            pets = objectMapper.readValue(response.body().toString(), new TypeReference<List<Pet>>() {
            });
            return pets;
        } catch (IOException e) {
            return new ArrayList<>();
        }
        }

    public List<Pet> findByTags(ClientHttp clientHttp, List<Tag> tags) throws IOException, InterruptedException {
        try {
            List<Pet> pets = new ArrayList<>();
            String endUrl = "/findByTags?";
            for (Tag tag : tags) {
                endUrl += "tags=" + tag.getName() + "&";
            }
            endUrl = endUrl.substring(0, endUrl.length() - 1);
            response = clientHttp.makeGetRequest(endUrl);
            pets = objectMapper.readValue(response.body().toString(), new TypeReference<List<Pet>>() {
            });
            return pets;
        } catch (IOException e) {
            return new ArrayList<>();
        }

    }

    public Pet findById(ClientHttp clientHttp, int id) throws IOException, InterruptedException {
        try {
            Pet pet = new Pet();
            response = clientHttp.makeGetRequest("/" + id);
            pet = objectMapper.readValue(response.body().toString(), Pet.class);
            return pet;
        } catch (IOException e) {
           // e.printStackTrace();
            return new Pet();
        }
    }

    public void delete(ClientHttp clientHttp, int id) throws IOException, InterruptedException {
        response = clientHttp.makeDeleteRequest("/" + id);
    }

    public int getStatusCode() {
        if (response != null) {
            return response.statusCode();
        } else {
            return 0;
        }
    }
}
