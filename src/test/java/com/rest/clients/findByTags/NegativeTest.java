package com.rest.clients.findByTags;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

public class NegativeTest {
    @ParameterizedTest
    @MethodSource("stringListProvider")
    void test1(List<Tag> tags) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        clientPet.findByTags(clientHttp, tags);
        int statusCode = 400;
        Assertions.assertEquals(statusCode,clientPet.getStatusCode());
    }

    static Stream<Arguments> stringListProvider() {
        return Stream.of(
                arguments(Arrays.asList(new Tag(1, "#")))
        );
    }
}
