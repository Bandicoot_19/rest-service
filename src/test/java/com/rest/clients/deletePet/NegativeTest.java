package com.rest.clients.deletePet;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.http.HttpResponse;

public class NegativeTest {
    @ParameterizedTest
    @ValueSource(strings = {"10987665"})
    void test1(String id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeGetRequest("/" + id);
        int statusCode = 404;
        Assertions.assertEquals(statusCode,response.statusCode());
    }
    @ParameterizedTest
    @ValueSource(strings = {"", "#1"})
    void test2(String id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeGetRequest("/" + id);
        int statusCode = 405;
        Assertions.assertEquals(statusCode,response.statusCode());
    }
}
