package com.rest.clients.deletePet;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;

public class PositiveTest {

    private static final int ID = 1;

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(ID, new Category(1, "dogs"), "Malysh", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(0, "string")), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
    }

    @Test
    void test1() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        clientPet.delete(clientHttp, ID);
        int statusCode = 200;
        Assertions.assertEquals(statusCode, clientPet.getStatusCode());
    }

    @Test
    void test2() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeDeleteRequest("/" + "aaa");
        int statusCode = 400;
        Assertions.assertEquals(statusCode, response.statusCode());
    }
}
