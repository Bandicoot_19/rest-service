package com.rest.clients.findByStatus;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.innerObjects.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.io.IOException;

public class PositiveTest {

    @ParameterizedTest
    @EnumSource(Status.class)
    void test1(Status status) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        clientPet.findByStatus(clientHttp, status.getValue());
        int statusCode = 200;
        Assertions.assertEquals(200,clientPet.getStatusCode());
    }
}
