package com.rest.clients.findByStatus;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.http.HttpResponse;

public class NegativeTest {
    @ParameterizedTest
    @ValueSource(strings = {"dd", "1"})
    void test1(String status) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeGetRequest("/findByStatus?status=" + status);
        int statusCode = 400;
        Assertions.assertEquals(statusCode,response.statusCode());
    }
}
