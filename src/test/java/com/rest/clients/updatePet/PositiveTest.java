package com.rest.clients.updatePet;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class PositiveTest {


    @Test
    void test1() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet1 = new Pet(10, new Category(1, "dogs"), "Jack", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(1, "colorBlack")), Status.SOLD.getValue());
        clientPet.addNew(clientHttp, pet1);
        Pet pet2 = new Pet(10, new Category(2, "dooog"), "Malysh", new ArrayList<String>(Arrays.asList("https://upload.wikimedia.org/wikipedia/en/thumb/f/fd/Droopy_dog.png/150px-Droopy_dog.png")), Arrays.asList(new Tag(2, "mops")), Status.SOLD.getValue());
        clientPet.Update(clientHttp, pet2);
        Pet pet3 = clientPet.findById(clientHttp, 10);
        Assertions.assertEquals(pet2, pet3); //not work
    }
}
