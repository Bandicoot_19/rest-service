package com.rest.clients.findById;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.http.HttpResponse;

public class NegativeTest {
    @ParameterizedTest
    @ValueSource(strings = {"foo", "&3", "(3)", "\"1\""/* not work */, "@1"})
    void test1(String id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeGetRequest("/" + id);
        int statusCode = 400;
        Assertions.assertEquals(statusCode,response.statusCode());
    }

    @ParameterizedTest
    @ValueSource(strings = {"#3", ""})
    void test2(String id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        HttpResponse response = clientHttp.makeGetRequest("/" + id);
        int statusCode = 405;
        Assertions.assertEquals(statusCode,response.statusCode());
    }
}
