package com.rest.clients.findById;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class PositiveTest {

    private static final int ID_IN_STOCK = 1;
    private static final int ID_ABSENT = 499987;

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(ID_IN_STOCK, new Category(1, "dogs"), "Malysh", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(0, "string")), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
        clientPet.delete(clientHttp, ID_ABSENT);
    }

    @Test
    void test1() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        clientPet.findById(clientHttp, ID_IN_STOCK);
        int statusCode = 200;
        Assertions.assertEquals(statusCode, clientPet.getStatusCode());
    }

    @Test
    void test2() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        clientPet.findById(clientHttp, ID_ABSENT);
        int statusCode = 404;
        Assertions.assertEquals(statusCode, clientPet.getStatusCode());
    }
}
