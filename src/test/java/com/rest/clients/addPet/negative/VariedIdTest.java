package com.rest.clients.addPet.negative;

import com.rest.clients.ClientHttp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.net.http.HttpResponse;

public class VariedIdTest {

    @ParameterizedTest
    @ValueSource(strings = {"j", "1v", "", "№5", "1,1", "1.1", "10000000000000000000000000"})
    void test1(String id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        String request = "{\n" +
                "  \"id\":" + id + "\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"category\": {\n" +
                "    \"id\": 1,\n" +
                "    \"name\": \"Dogs\"\n" +
                "  },\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";
        HttpResponse response = clientHttp.makePostRequest("", request);
        int statusCode = 400;
        Assertions.assertEquals(statusCode, response.statusCode());
    }
}
