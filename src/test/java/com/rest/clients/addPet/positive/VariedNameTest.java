package com.rest.clients.addPet.positive;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class VariedNameTest {

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(99, new Category(1, "dogs"), "Malysh", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(1, "colorBlack")), Status.SOLD.getValue());
        clientPet.addNew(clientHttp, pet);
    }
    @ParameterizedTest
    @ValueSource(strings = {"Malysh", "Малыш", "123", " ", "#doggie"})
    void test1(String name) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(10, new Category(1, "dogs"), name, new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(0, "string")), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
        int statusCode = 200;
        Assertions.assertEquals(statusCode,clientPet.getStatusCode());
    }

}
