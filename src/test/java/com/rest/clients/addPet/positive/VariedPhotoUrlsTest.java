package com.rest.clients.addPet.positive;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class VariedPhotoUrlsTest {

    @ParameterizedTest
    @ValueSource(strings = {"\"https://schulzmuseum.org/wp-content/uploads/2017/06/920608_FlyingAce-200.jpg\", \"https://www.calmuseums.org/images/snoopy/snoopy.png\", \"https://vignette.wikia.nocookie.net/peanuts/images/2/28/AmigosperdemoSono.png/revision/latest?cb=20110823202539\"", "https://vignette.wikia.nocookie.net/peanuts/images/2/28/AmigosperdemoSono.png/revision/latest?cb=20110823202539", "https://www.calmuseums.org/images/snoopy/snoopy.png", "https://schulzmuseum.org/wp-content/uploads/2017/06/920608_FlyingAce-200.jpg"})
    void test1(String photoUrls) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(10, new Category(1, "dogs"), "doggie", Collections.singletonList(photoUrls), Arrays.asList(new Tag(0, "string")), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
        int statusCode = 200;
        Assertions.assertEquals(200, clientPet.getStatusCode());
    }

}
