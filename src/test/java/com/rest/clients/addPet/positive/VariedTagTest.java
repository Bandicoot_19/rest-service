package com.rest.clients.addPet.positive;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.Category;
import com.rest.entities.innerObjects.Status;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class VariedTagTest {

    private static Stream<Arguments> TagsForIsBlank() {
        return Stream.of(
                Arguments.of(100000000, "[\"mops\"; \"%1mops\"]"),
                Arguments.of(1, "mops")
        );
    }

    @ParameterizedTest
    @MethodSource("TagsForIsBlank")
    void test1(double id, String name) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(1, new Category(id, "dogs"), "Malysh", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(id, name)), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
        int statusCode = 200;
        Assertions.assertEquals(200,clientPet.getStatusCode());
    }
}
