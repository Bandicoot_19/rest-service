package com.rest.clients.addPet.positive;

import com.rest.clients.ClientHttp;
import com.rest.clients.ClientPet;
import com.rest.entities.Pet;
import com.rest.entities.innerObjects.*;
import com.rest.entities.innerObjects.Tag;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class VariedIdTest {

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(99, new Category(1, "dogs"), "Jack", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(1, "colorBlack")), Status.SOLD.getValue());
        clientPet.addNew(clientHttp, pet);
    }

    @ParameterizedTest
    @ValueSource(ints = {8, -1, -1000000, 0, 5, 99, 100000000})
    void test1(int id) throws IOException, InterruptedException {
        ClientHttp clientHttp = new ClientHttp();
        ClientPet clientPet = new ClientPet();
        Pet pet = new Pet(id, new Category(1, "dogs"), "doggie", new ArrayList<String>(Arrays.asList("string")), Arrays.asList(new Tag(0, "string")), Status.AVAILABLE.getValue());
        clientPet.addNew(clientHttp, pet);
        int statusCode = 200;
        Assertions.assertEquals(statusCode,clientPet.getStatusCode());
        clientPet.delete(clientHttp, id); // это уже не относится к тесту, но как мне удалять с тем же набором данных?
    }
}
